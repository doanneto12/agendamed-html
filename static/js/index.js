jQuery(function( $ ){
	$(window).scroll(function () {
		if ($(document).scrollTop() > 30 ) {
			$('header#header-section').addClass('sticky-header');
		} 
		else {
			$('header#header-section').removeClass('sticky-header');
		}
	});
});