from flask import Flask, render_template
app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login')
def login():
    return render_template('login.html')
    
@app.route('/nosotros')
def nosotros():
    return render_template('nosotros.html')



@app.route('/registro')
def registro():
    return render_template('registro.html')

@app.route('/agendarcitas')
def agendarcitas():
    return render_template('agendarcitas.html')

@app.route('/consultarcitas')
def consultarcitas():
    return render_template('consultarcitas.html')

@app.route('/consultarformula')
def consultarformula():
    return render_template('consultarformula.html')

@app.route('/servicio')
def servicio():
    return render_template('servicio.html')


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=7000, debug=True)
 